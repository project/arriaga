About Arriaga Theme
====================
Arriaga Theme is clean, modern, elegant and responsive theme for Drupal 8.x by www.kattio.ru. 
This theme is suitable for all types of websites, from corporate site to blog site, and it works perfectly in every browser.
The theme is not dependent on any core theme. Its very light weight for fast loading with modern look.

Features
Responsive, SEO-friendly Theme
Clean, modern and functional Design
1, 2 or 3 column layout
Flex Image Slideshow with Caption (Customizable)
Mobile support (Smartphone, Tablet, Android, iPhone, etc)
Multi-level drop-down menus (Multilingual menu)
HTML5 & super clean markup
A total of 15 block regions
Drupal standards compliant and Supported standard theme features
Google Font and nice typography
Ideal for a business, company, blog, portfolio or a variety of other websites.
Easy for users to understand and make navigating your website a pleasure
Detailed CSS rules for Typography, Forms Elements, Node Teaser, Comments, etc.
Design optimization for SEO

Browser compatibility:
=====================
The theme has been tested on following browsers. IE7+, Firefox, Google Chrome, Opera.

Drupal compatibility:
=====================
This theme is compatible with Drupal 8.x.x

Developed by
=====================================
https://www.drupal.org/u/katrin
https://www.kattio.ru


